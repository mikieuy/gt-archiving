﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.SP.DocumentArchivingLibrary
{
    public class FieldRef
    {
        public Guid? ID { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public bool? IsRequired { get; set; }
        public bool? IsHidden { get; set; }
        public bool? IsReadOnly { get; set; }

        public string Aggregation { get; set; }
        public string Customization { get; set; }
        public string PIAttribute { get; set; }
        public string PITarget { get; set; }
        public string PrimaryPIAttribute { get; set; }
        public string PrimaryPITarget { get; set; }
        public string Node { get; set; }
    }
}
