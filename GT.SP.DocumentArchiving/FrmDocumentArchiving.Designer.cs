﻿namespace GT.SP.DocumentArchiving
{
    partial class FrmDocumentArchiving
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.btnStartArchiving = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.lblStatus = new System.Windows.Forms.Label();
            this.btnNotifyExpired = new System.Windows.Forms.Button();
            this.btnNotifyArchiving = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // btnStartArchiving
            // 
            this.btnStartArchiving.Location = new System.Drawing.Point(12, 12);
            this.btnStartArchiving.Name = "btnStartArchiving";
            this.btnStartArchiving.Size = new System.Drawing.Size(105, 23);
            this.btnStartArchiving.TabIndex = 0;
            this.btnStartArchiving.Text = "Start Archiving";
            this.btnStartArchiving.UseVisualStyleBackColor = true;
            this.btnStartArchiving.Click += new System.EventHandler(this.btnStartArchiving_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(12, 49);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 13);
            this.lblStatus.TabIndex = 1;
            // 
            // btnNotifyExpired
            // 
            this.btnNotifyExpired.Location = new System.Drawing.Point(123, 12);
            this.btnNotifyExpired.Name = "btnNotifyExpired";
            this.btnNotifyExpired.Size = new System.Drawing.Size(149, 23);
            this.btnNotifyExpired.TabIndex = 2;
            this.btnNotifyExpired.Text = "Start Expired Notification";
            this.btnNotifyExpired.UseVisualStyleBackColor = true;
            this.btnNotifyExpired.Click += new System.EventHandler(this.btnNotifyExpired_Click);
            // 
            // btnNotifyArchiving
            // 
            this.btnNotifyArchiving.Location = new System.Drawing.Point(279, 11);
            this.btnNotifyArchiving.Name = "btnNotifyArchiving";
            this.btnNotifyArchiving.Size = new System.Drawing.Size(170, 23);
            this.btnNotifyArchiving.TabIndex = 3;
            this.btnNotifyArchiving.Text = "Start Archiving Notification";
            this.btnNotifyArchiving.UseVisualStyleBackColor = true;
            this.btnNotifyArchiving.Click += new System.EventHandler(this.btnNotifyArchiving_Click);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.Text = "notifyIcon1";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // FrmDocumentArchiving
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(458, 76);
            this.Controls.Add(this.btnNotifyArchiving);
            this.Controls.Add(this.btnNotifyExpired);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.btnStartArchiving);
            this.Name = "FrmDocumentArchiving";
            this.Text = "Document Archiving";
            this.Load += new System.EventHandler(this.FrmArchivingDocument_Load);
            this.Resize += new System.EventHandler(this.FrmDocumentArchiving_Resize);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStartArchiving;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Button btnNotifyExpired;
        private System.Windows.Forms.Button btnNotifyArchiving;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

