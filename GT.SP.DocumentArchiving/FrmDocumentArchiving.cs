﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.SharePoint;
using System.Configuration;
using Microsoft.SharePoint.Utilities;
using System.Net.Mail;
using System.Collections;

namespace GT.SP.DocumentArchiving
{
    public partial class FrmDocumentArchiving : Form
    {
        #region Constructors

        public FrmDocumentArchiving()
        {
            InitializeComponent();
            int interval = 60000; //in ms

            try
            {
                string intervalConfig = ConfigurationManager.AppSettings["Interval"];
                interval = int.Parse(intervalConfig);
            }
            catch (Exception ex)
            {
                interval = 60000;
            }

            timer1.Interval = interval;
            timer1.Start();
        }
        private void timer1_Tick(object sender, EventArgs e)
        {
            DateTime datenow = DateTime.Now;

            int hour = 0;
            int minute = 0;

            try
            {
                string startHour = ConfigurationManager.AppSettings["StartHour"];
                hour = int.Parse(startHour);
            }
            catch (Exception ex)
            {
                hour = 0;
            }

            try
            {
                string startMinute = ConfigurationManager.AppSettings["StartMinute"];
                minute = int.Parse(startMinute);
            }
            catch (Exception ex)
            {
                minute = 0;
            }

            if(datenow.Hour == hour && datenow.Minute == minute)
            {
                ArchiveDocument();
                RunExpiredNotification();
                RunArchivedNotification();
            }
        }

        #endregion

        #region Event Handlers

        private void FrmArchivingDocument_Load(object sender, EventArgs e)
        {

        }

        private void btnStartArchiving_Click(object sender, EventArgs e)
        {
            ArchiveDocument();
        }

        private void btnNotifyExpired_Click(object sender, EventArgs e)
        {
            RunExpiredNotification();
        }

        private void btnNotifyArchiving_Click(object sender, EventArgs e)
        {
            RunArchivedNotification();
        }

        #endregion

        #region Helper Functions

        void writeLog(string filename, string value)
        {
            StreamWriter sw;
            try
            {
                if (!File.Exists(filename))
                {
                    sw = new StreamWriter(filename, true);
                }
                else
                {
                    sw = File.AppendText(filename);
                }
                sw.WriteLine(DateTime.Now + " : " + value);
            }
            catch (Exception e)
            {
                sw = new StreamWriter(filename, true);
                sw.WriteLine(DateTime.Now + " : " + e.Message + ", " + e.StackTrace);
            }
            sw.Close();
        }

        protected void sendMail(string Subject, string Body, string MailFrom, string Password, string[] MailTo)
        {
            try
            {
                MailMessage mail = new MailMessage();
                mail.From = new MailAddress(MailFrom);
                foreach (string mailto in MailTo)
                {
                    mail.To.Add(mailto);
                }
                mail.Subject = Subject;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                string smtpClient = ConfigurationManager.AppSettings["SMTPClient"];
                SmtpClient client = new SmtpClient();
                client.Host = smtpClient;
                client.UseDefaultCredentials = false;
                client.Credentials = new System.Net.NetworkCredential(MailFrom, Password);
                //client.Port = 587;
                //client.EnableSsl = true;

                client.Send(mail);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #region Archive and Notification Handler Methods
        protected void NotifyExpiredDocument(int day)
        {
            StringBuilder sb = new StringBuilder();
            string logfolder = ConfigurationManager.AppSettings["LogFolder"];
            string logfile = logfolder + "\\Notify Expired Document - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + " log.txt";
            try
            {
                //string SiteURL = ConfigurationManager.AppSettings["SiteURL" + identifier];
                //string SiteURLArchive = ConfigurationManager.AppSettings["SiteURL" + identifier + "Archive"];
                string SiteURL = ConfigurationManager.AppSettings["SiteURL"];
                string SiteURLArchiving = ConfigurationManager.AppSettings["SiteURLArchiving"];
                string ListSettingArchiveDocument = ConfigurationManager.AppSettings["ListSettingArchiveDocument"];
                string listArchivingSitesName = ConfigurationManager.AppSettings["ListArchivingSites"];
                string contentType = string.Empty;

                #region Define data table to store expired documents
                DateTime todaydate = DateTime.Now;
                DateTime notifydate = todaydate.AddDays(day);
                //writeLog(logfile, "Debug - " + notifydate.ToString());
                DataTable listdokumen = new DataTable();
                listdokumen.Columns.Add("File");
                listdokumen.Columns.Add("Folder");
                listdokumen.Columns.Add("Link");
                listdokumen.Columns.Add("MailTo");
                listdokumen.Columns.Add("URL");
                listdokumen.Columns.Add("Subsite");
                #endregion

                string MailFrom = ConfigurationManager.AppSettings["MailFrom"];
                string Password = ConfigurationManager.AppSettings["Password"];
                List<string> MailTo = new List<string>();

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite intranetSite = new SPSite(SiteURL))
                    {
                        //writeLog(logfile, "Debug - 1");
                        using (SPWeb intranetWeb = intranetSite.OpenWeb())
                        {
                            SPList listArchivingSites = intranetWeb.Lists[listArchivingSitesName];
                            // Get All Sites which implement archiving document feature
                            SPQuery camlQuery = new SPQuery();
                            SPListItemCollection sitesWhoImplementArchiving = listArchivingSites.GetItems(camlQuery);

                            foreach (SPListItem site in sitesWhoImplementArchiving)
                            {
                                SPFieldUrlValue siteUrl = new SPFieldUrlValue(site["Site URL"].ToString());
                                SPFieldUrlValue archivingSiteUrl = new SPFieldUrlValue(site["Archiving Site URL"].ToString());

                                string doclibSiteUrl = siteUrl.Url;
                                string doclibArchivingSiteUrl = archivingSiteUrl.Url;

                                using (SPSite doclibSite = new SPSite(doclibSiteUrl))
                                {
                                    using (SPWeb doclibWeb = doclibSite.OpenWeb())
                                    {
                                        //writeLog(logfile, "Debug - 2");
                                        SPList ListSetting = doclibWeb.Lists[ListSettingArchiveDocument];
                                        //writeLog(logfile, "Debug - 2a"); 
                                        foreach (SPListItem setting in ListSetting.Items)
                                        {
                                            try
                                            {
                                                #region Get Email Recipients
                                                string doclib = setting["Title"].ToString();
                                                string notifikasi = setting["Notifikasi Email"].ToString();
                                                //writeLog(logfile, "Debug - 2b"); 
                                                string mailto = "";
                                                var penerimaemail = (SPFieldUserValueCollection)setting["Penerima Email"];
                                                foreach (SPFieldUserValue penerima in penerimaemail)
                                                {
                                                    if (String.IsNullOrEmpty(mailto))
                                                        mailto = penerima.User.Email;
                                                    else
                                                        mailto += ";" + penerima.User.Email;
                                                }
                                                //writeLog(logfile, "Debug - 2c"); 
                                                if (notifikasi.Equals("True"))
                                                {
                                                    SPList CobjLists = doclibWeb.Lists[doclib.Replace("%20", " ")];
                                                    //writeLog(logfile, "Debug - 2d"); 
                                                    foreach (SPContentType ContentType in CobjLists.ContentTypes)
                                                    {
                                                        try
                                                        {
                                                            #region Get All Expired Documents
                                                            contentType = ContentType.Name;
                                                            if (!contentType.Equals("Document") && !contentType.Equals("Folder"))
                                                            {
                                                                writeLog(logfile, "Start Sending Expired Date Notification for " + contentType + " documents.");
                                                                lblStatus.Text = "Start Send Notification Expired Date " + contentType + " Document...";
                                                                writeLog(logfile, "Start Send Notification Expired Date " + contentType + " Document - " + todaydate.ToShortDateString());
                                                                //writeLog(logfile, "Debug - " + ContentType.Name);
                                                                //writeLog(logfile, "Debug - 3");
                                                                SPListItemCollection CobjItemcoll;
                                                                SPQuery CobjQuery = new SPQuery();

                                                                CobjQuery.ViewAttributes = "Scope='Recursive'";
                                                                String Cstrquerry = "";
                                                                Cstrquerry = @"  <Where>
                                                                                    <And>
                                                                                        <And>
                                                                                            <Eq>
                                                                                                <FieldRef Name='Expired_x0020_Date' />
                                                                                                <Value Type='DateTime' IncludeTimeValue='False'>keyword</Value>
                                                                                            </Eq>
                                                                                            <Neq>
                                                                                                <FieldRef Name='columnname' />
                                                                                                <Value Type='DateTime' IncludeTimeValue='False'>keyword</Value>
                                                                                            </Neq>
                                                                                        </And>
                                                                                        <Eq>
                                                                                            <FieldRef Name='ContentType' />
                                                                                            <Value Type='Computed'>"+ contentType +@"</Value>
                                                                                        </Eq>   
                                                                                    </And>                           
                                                                                </Where>";

                                                                string columnname = "Archived Date";// + contenttype;
                                                                                                    //writeLog(logfile, "Debug - " + columnname);
                                                                Cstrquerry = Cstrquerry.Replace("columnname", columnname.Replace(" ", "_x0020_"));
                                                                Cstrquerry = Cstrquerry.Replace("keyword", SPUtility.CreateISO8601DateTimeFromSystemDateTime(notifydate));
                                                                CobjQuery.Query = Cstrquerry;
                                                                //writeLog(logfile, "Debug - 4");
                                                                //writeLog(logfile, "Query: " + Cstrquerry);

                                                                CobjItemcoll = CobjLists.GetItems(CobjQuery);
                                                                //writeLog(logfile, "Debug - 5");

                                                                foreach (SPListItem doc in CobjItemcoll)
                                                                {
                                                                    try
                                                                    {
                                                                        string file = doc.File.Name;
                                                                        string folder = doc.File.Url.Substring(0, doc.File.Url.LastIndexOf('/'));
                                                                        string link = doclibWeb.Url + "/" + doc.File.Url;
                                                                        listdokumen.Rows.Add(file, folder, link, mailto, doclibWeb);
                                                                        //writeLog(logfile, "Debug - " + file + folder + link);
                                                                        //writeLog(logfile, "Debug - 6");
                                                                        writeLog(logfile, doc.File.Name + " akan kadaluarsa dalam " + day + " hari");
                                                                    }
                                                                    catch (Exception ex)
                                                                    {
                                                                        writeLog(logfile, doc.File.Name + ": " + ex.Message);
                                                                    }
                                                                }
                                                            }
                                                            #endregion
                                                            //writeLog(logfile, "Debug - 7");

                                                            #region Send Notification Email
                                                            if (listdokumen.Rows.Count > 0)
                                                            {
                                                                List<DataTable> listdokumenbypenerima = listdokumen.AsEnumerable()
                                                                                        .GroupBy(row => row.Field<string>("MailTo"))
                                                                                        .Select(g => g.CopyToDataTable())
                                                                                        .ToList();
                                                                foreach (DataTable dokumenbypenerima in listdokumenbypenerima)
                                                                {
                                                                    MailTo = dokumenbypenerima.Rows[0]["MailTo"].ToString().Split(';').ToList();
                                                                    string Subject = "Email Notifikasi Expired Date";
                                                                    string Body = "<b>Dear " + contentType + " Team,</b>" +
                                                                                "<br></br>" +
                                                                                "Berikut list Dokumen yang akan Expired dalam waktu " + day + " hari kedepan :" +
                                                                                "<br></br>";
                                                                    Body += "<table border=\"1\">" +
                                                                            "<tr>" +
                                                                            "<td>No.</td>" +
                                                                            "<td>File</td>" +
                                                                            "<td>Folder</td>" +
                                                                            "<td>Link</td>" +
                                                                            "</tr>";
                                                                    int count = 0;
                                                                    writeLog(logfile, "List of expired documents: ");
                                                                    foreach (DataRow dokumen in dokumenbypenerima.Rows)
                                                                    {
                                                                        count++;
                                                                        Body += "<tr>";
                                                                        Body += "<td>" + count.ToString() + ". </td>";
                                                                        Body += "<td>" + dokumen["File"] + "</td>";
                                                                        Body += "<td>" + dokumen["Folder"] + "</td>";
                                                                        Body += "<td><a href=\"" + dokumen["Link"] + "\">" + dokumen["Link"] + "</a></td>";
                                                                        Body += "</tr>";
                                                                        writeLog(logfile, dokumen["Link"].ToString());
                                                                    }
                                                                    Body += "</table><br></br>";
                                                                    Body += "Klik link berikut untuk membuka " + contentType + " Sub-site :" +
                                                                                "<br></br>" +
                                                                                "<a href='" + doclibWeb.Url + "'>" + doclibWeb.Title + "</a>" +
                                                                                "<br></br>" +
                                                                                "<i>GT Document Management System Sharepoint</i>";
                                                                    sendMail(Subject, Body, MailFrom, Password, MailTo.ToArray());
                                                                    //writeLog(logfile, Body);
                                                                }
                                                            }
                                                            //writeLog(logfile, "Debug - 10");
                                                            writeLog(logfile, "Send Notification Expired Date " + contentType + " Document Success");
                                                            lblStatus.Text = "Send Notification Expired Date " + contentType + " Document Success";

                                                            listdokumen.Clear();
                                                            #endregion
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                            writeLog(logfile, ContentType.Name + ": " + ex.Message);
                                                        }
                                                    }
                                                }
                                                #endregion
                                            }
                                            catch (Exception ex)
                                            {
                                                writeLog(logfile, setting["Title"].ToString() + ": " + ex.Message);
                                            }
                                        }
                                    }
                                }
                            }
                            //writeLog(logfile, "Debug - 9");                            
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                writeLog(logfile, ex.Message);
                lblStatus.Text = "Send Notification Expired Document Failed : " + ex.Message;
            }
        }

        protected void ArchiveDocument()
        {
            StringBuilder sb = new StringBuilder();
            string logfolder = ConfigurationManager.AppSettings["LogFolder"];
            string logfile = logfolder + "\\Archive Document - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + " log.txt";
            string contentType = string.Empty;
            try
            {
                string SiteURL = ConfigurationManager.AppSettings["SiteURL"];
                string SiteURLArchive = ConfigurationManager.AppSettings["SiteURLArchiving"];
                string ListSettingArchiveDocument = ConfigurationManager.AppSettings["ListSettingArchiveDocument"];
                string listArchivingSitesName = ConfigurationManager.AppSettings["ListArchivingSites"];
                DateTime todaydate = DateTime.Now;

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    #region Open Root Site
                    using (SPSite rootSite = new SPSite(SiteURL))
                    {
                        //writeLog(logfile, "Debug - 1");
                        using (SPWeb rootWeb = rootSite.OpenWeb())
                        {
                            #region Get All sites who implement archiving feature
                            SPList listArchivingSites = rootWeb.Lists[listArchivingSitesName];
                            // Get All Sites which implement archiving document feature
                            SPQuery camlQuery = new SPQuery();
                            SPListItemCollection sitesWhoImplementArchiving = listArchivingSites.GetItems(camlQuery);

                            #region Get all sites who implement archiving feature
                            foreach (SPListItem site in sitesWhoImplementArchiving)
                            {
                                SPFieldUrlValue siteUrl = new SPFieldUrlValue(site["Site URL"].ToString());
                                SPFieldUrlValue archivingSiteUrl = new SPFieldUrlValue(site["Archiving Site URL"].ToString());

                                string doclibSiteUrl = siteUrl.Url;
                                string doclibArchivingSiteUrl = archivingSiteUrl.Url;

                                #region Open Production Site
                                using (SPSite intranetSite = new SPSite(doclibSiteUrl))
                                {
                                    using (SPWeb intranetWeb = intranetSite.OpenWeb())
                                    {
                                        //writeLog(logfile, "Debug - 2");      
                                        #region Open Archiving Site
                                        using (SPSite intranetSiteArchive = new SPSite(doclibArchivingSiteUrl))
                                        {
                                            //writeLog(logfile, "Debug - 2a");   
                                            using (SPWeb intranetWebArchive = intranetSiteArchive.OpenWeb())
                                            {
                                                #region Get all document library which implements archiving feature
                                                SPList ListSetting = intranetWeb.Lists[ListSettingArchiveDocument];
                                                //sb.AppendLine("Debug - 2b");
                                                foreach (SPListItem setting in ListSetting.Items)
                                                {
                                                    try
                                                    {
                                                        string doclib = setting["Title"].ToString();
                                                        SPList CobjLists = intranetWeb.Lists[doclib.Replace("%20", " ")];
                                                        foreach (SPContentType ContentType in CobjLists.ContentTypes)
                                                        {
                                                            try
                                                            {
                                                                contentType = ContentType.Name;
                                                                lblStatus.Text = "Start " + contentType + " Document...";
                                                                writeLog(logfile, "Start Archive " + contentType + " Document - " + todaydate.ToShortDateString());
                                                                if (!contentType.Equals("Folder"))
                                                                {
                                                                    #region Get all documents whose archived date is less than or equal to today
                                                                    //writeLog(logfile, "Debug - 3");
                                                                    SPListItemCollection CobjItemcoll;
                                                                    SPQuery CobjQuery = new SPQuery();

                                                                    CobjQuery.ViewAttributes = "Scope='Recursive'";
                                                                    String Cstrquerry = "";
                                                                    Cstrquerry = @"  <Where>
                                                                                        <Leq>
                                                                                            <FieldRef Name='columnname' />
                                                                                            <Value Type='DateTime' IncludeTimeValue='False'>keyword</Value>
                                                                                        </Leq>                                      
                                                                                    </Where>";
                                                                    string columnname = "Archived Date";// + contenttype;
                                                                                                        //writeLog(logfile, "Debug - " + columnname);
                                                                    Cstrquerry = Cstrquerry.Replace("columnname", columnname.Replace(" ", "_x0020_"));
                                                                    Cstrquerry = Cstrquerry.Replace("keyword", SPUtility.CreateISO8601DateTimeFromSystemDateTime(todaydate));
                                                                    CobjQuery.Query = Cstrquerry;
                                                                    //writeLog(logfile, "Debug - 4");

                                                                    CobjItemcoll = CobjLists.GetItems(CobjQuery);
                                                                    //writeLog(logfile, "Debug - 5");
                                                                    #endregion

                                                                    #region Iterate through each list item, and archive the document
                                                                    List<int> successCount = new List<int>();
                                                                    int success = 0;
                                                                    foreach (SPListItem doc in CobjItemcoll)
                                                                    {
                                                                        try
                                                                        {
                                                                            #region Create list on the archiving site, if the list is not existed yet on the archiving site
                                                                            writeLog(logfile, "- Start Archive file :" + doc.File.Url);
                                                                            //Update Metadata
                                                                            intranetWeb.AllowUnsafeUpdates = true;
                                                                            intranetWebArchive.AllowUnsafeUpdates = true;
                                                                            intranetSiteArchive.RootWeb.AllowUnsafeUpdates = true;
                                                                            //writeLog(logfile, "Debug - 6");
                                                                            SPList destinationList;
                                                                            if (intranetWebArchive.Lists.TryGetList(doclib.Replace("%20", " ")) == null)
                                                                            {
                                                                                intranetWebArchive.Lists.Add(doclib.Replace("%20", " "), doclib + " Archive Library", SPListTemplateType.DocumentLibrary);
                                                                                intranetWebArchive.Update();
                                                                            }

                                                                            destinationList = intranetWebArchive.Lists[doclib.Replace("%20", " ")];
                                                                            destinationList.ContentTypesEnabled = true;
                                                                            
                                                                            string urldoc = doc.File.Url;
                                                                            urldoc = urldoc.Substring((doclib + "/").Length);
                                                                            if (urldoc.Contains('/'))
                                                                            {
                                                                                urldoc = urldoc.Substring(0, urldoc.LastIndexOf('/'));
                                                                                string[] urlfolder = urldoc.Split('/');
                                                                                if (urlfolder.Count() > 0)
                                                                                {
                                                                                    SPFolder folder = intranetWebArchive.GetFolder(doclib);
                                                                                    foreach (string urlfol in urlfolder)
                                                                                    {
                                                                                        try
                                                                                        {
                                                                                            folder = folder.SubFolders[urlfol];
                                                                                        }
                                                                                        catch
                                                                                        {
                                                                                            folder.SubFolders.Add(urlfol);
                                                                                            folder = folder.SubFolders[urlfol];
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }

                                                                            #endregion

                                                                            #region Check and Add Content Types to Document Library (if content types do not exist on the subsite, add new content type locally to the doc lib)
                                                                            SPContentTypeId contentTypeId;
                                                                            SPContentType ctype;
                                                                            string contentTypeName = string.Empty;
                                                                            if (destinationList.ContentTypes[doc.ContentType.Name] == null)
                                                                            {
                                                                                ctype = CobjLists.ContentTypes[doc.ContentType.Name];
                                                                                SPContentType destinationContentType = ctype;
                                                                                SPContentType tempContentType = intranetWebArchive.AvailableContentTypes[ctype.Name];
                                                                                if(tempContentType != null)
                                                                                {
                                                                                    // If content type exists on the site, use it instead
                                                                                    destinationContentType = tempContentType;
                                                                                }
                                                                                else
                                                                                {
                                                                                    // If content type does not exist yet on the site, add it
                                                                                    destinationContentType = intranetWebArchive.ContentTypes.Add(ctype);
                                                                                    intranetWebArchive.Update();
                                                                                }
                                                                                destinationList.ContentTypes.Add(destinationContentType);
                                                                                destinationList.Update();

                                                                                contentTypeId = destinationContentType.Id;
                                                                                contentTypeName = destinationContentType.Name;
                                                                            }
                                                                            else
                                                                            {
                                                                                ctype = destinationList.ContentTypes[doc.ContentType.Name];
                                                                                contentTypeId = ctype.Id;
                                                                                contentTypeName = ctype.Name;
                                                                            }
                                                                            #endregion

                                                                            #region Check And add fields by comparing Destination Content Type and Source Content Type (if they don't exist)
                                                                            SPContentType archivingContentType = destinationList.ContentTypes[contentTypeName];
                                                                            SPField tempField;
                                                                            SPFieldLink tempFieldLink;
                                                                            if (archivingContentType != null)
                                                                            {
                                                                                foreach (SPField field in ContentType.Fields)
                                                                                {
                                                                                    if (!archivingContentType.Fields.ContainsFieldWithStaticName(field.StaticName))
                                                                                    {
                                                                                        if (!destinationList.Fields.ContainsFieldWithStaticName(field.StaticName))
                                                                                        {
                                                                                            destinationList.Fields.Add(field);
                                                                                            destinationList.Update();
                                                                                        }

                                                                                        if (!destinationList.ContentTypes[contentTypeName].Fields.ContainsFieldWithStaticName(field.StaticName))
                                                                                        {
                                                                                            tempField = destinationList.Fields[field.Title];
                                                                                            tempFieldLink = new SPFieldLink(tempField);
                                                                                            destinationList.ContentTypes[contentTypeName].FieldLinks.Add(tempFieldLink);
                                                                                            destinationList.ContentTypes[contentTypeName].Update();
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                            #endregion

                                                                            #region Assign values to each document on archiving site
                                                                            SPContentType cType = destinationList.ContentTypes[doc.ContentType.Name];

                                                                            Hashtable metadata = new Hashtable();
                                                                            metadata.Add("ContentType", doc["ContentType"]);

                                                                            //copy sourceItem to destinationList
                                                                            SPFile targetItem = intranetWebArchive.Files.Add(doclibArchivingSiteUrl + "/" + doc.File.Url, doc.File.OpenBinary(), metadata, true);
                                                                            SPListItem docTarget = targetItem.Item;

                                                                            foreach (SPField f in cType.Fields)
                                                                            {
                                                                                try
                                                                                {
                                                                                    if (!f.ReadOnlyField)
                                                                                    {
                                                                                        if (f.Title != "Name" && f.Title != "Content Type")
                                                                                        {
                                                                                            if(docTarget.ContentType.Fields.ContainsFieldWithStaticName(f.StaticName))
                                                                                                docTarget[f.InternalName] = doc[f.InternalName];
                                                                                        }
                                                                                    }
                                                                                }
                                                                                catch (Exception ex)
                                                                                {
                                                                                    writeLog(logfile, f.Title + ": " + ex.Message);
                                                                                }
                                                                            }
                                                                            docTarget.Update();
                                                                            if (targetItem.CheckOutType != SPFile.SPCheckOutType.None)
                                                                            {
                                                                                targetItem.CheckIn("Check in", SPCheckinType.MajorCheckIn);
                                                                            }
                                                                            //writeLog(logfile, "Debug - 7");      
                                                                            intranetSiteArchive.RootWeb.AllowUnsafeUpdates = false;      
                                                                            intranetWebArchive.AllowUnsafeUpdates = false;                                
                                                                            intranetWeb.AllowUnsafeUpdates = false;
                                                                            writeLog(logfile, "- Success Archive file :" + doc.File.Url);
                                                                            //writeLog(logfile, "Debug - 8");
                                                                            successCount.Add(success);
                                                                            success++;

                                                                            #endregion
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            writeLog(logfile, doc.File.Url + ": " + ex.Message);
                                                                        }
                                                                    }
                                                                    #endregion

                                                                    #region Delete the document from production site
                                                                    //writeLog(logfile, "Debug - 9");
                                                                    intranetWeb.AllowUnsafeUpdates = true;
                                                                    int count = CobjItemcoll.Count;
                                                                    for (int x = count - 1; x >= 0; x--)
                                                                    {
                                                                        if (successCount.Contains(x))
                                                                        {
                                                                            CobjItemcoll.Delete(x);
                                                                        }
                                                                    }
                                                                    intranetWeb.AllowUnsafeUpdates = false;
                                                                    //writeLog(logfile, "Debug - 9a");

                                                                    //writeLog(logfile, "Debug - 10");
                                                                    writeLog(logfile, "Archive " + contentType + " Success");
                                                                    lblStatus.Text = "Archive " + contentType + " Success";
                                                                    #endregion
                                                                }
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                writeLog(logfile, ContentType.Name + ": " + ex.Message);
                                                            }
                                                        }
                                                    }
                                                    catch (Exception ex)
                                                    {
                                                        writeLog(logfile, setting["Title"].ToString() + ": " + ex.Message);
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                #endregion
                            }
                            #endregion

                            #endregion
                        }
                    }
                    #endregion
                });
            }
            catch (Exception ex)
            {
                writeLog(logfile, ex.Message);
                lblStatus.Text = "Archive Legal Document Failed : " + ex.Message;
            }
        }

        protected void NotifyArchiveDocument(int day)
        {
            StringBuilder sb = new StringBuilder();
            string logfolder = ConfigurationManager.AppSettings["LogFolder"];
            string logfile = logfolder + "\\Notify Archive Document - " + DateTime.Now.ToString("yyyyMMdd_hhmmss") + " log.txt";
            string contenttype = string.Empty;
            try
            {
                string SiteURL = ConfigurationManager.AppSettings["SiteURL"];
                string SiteURLArchive = ConfigurationManager.AppSettings["SiteURLArchiving"];
                string ListSettingArchiveDocument = ConfigurationManager.AppSettings["ListSettingArchiveDocument"];
                string listArchivingSitesName = ConfigurationManager.AppSettings["ListArchivingSites"];
                DateTime todaydate = DateTime.Now;

                #region Define datatable to store archived documents
                DateTime notifydate = todaydate.AddDays(day);
                //writeLog(logfile, "Debug - " + notifydate.ToString());
                DataTable listdokumen = new DataTable();
                listdokumen.Columns.Add("File");
                listdokumen.Columns.Add("Folder");
                listdokumen.Columns.Add("Link");
                listdokumen.Columns.Add("MailTo");

                string MailFrom = ConfigurationManager.AppSettings["MailFrom"];
                string Password = ConfigurationManager.AppSettings["Password"];
                List<string> MailTo = new List<string>();

                #endregion

                SPSecurity.RunWithElevatedPrivileges(delegate ()
                {
                    using (SPSite rootSite = new SPSite(SiteURL))
                    {
                        using (SPWeb rootWeb = rootSite.OpenWeb())
                        {
                            #region Get all archiving sites
                            SPList listArchivingSite = rootWeb.Lists[listArchivingSitesName];
                            if(listArchivingSite != null)
                            {
                                SPListItemCollection sitesWhoImplementArchiving = listArchivingSite.GetItems();
                                string siteUrl;
                                string archivingSiteUrl;
                                foreach(SPListItem siteWhoImplementArchiving in sitesWhoImplementArchiving)
                                {
                                    siteUrl = new SPFieldUrlValue(siteWhoImplementArchiving["Site URL"].ToString()).Url;
                                    archivingSiteUrl = new SPFieldUrlValue(siteWhoImplementArchiving["Archiving Site URL"].ToString()).Url;

                                    #region Open subsite
                                    using (SPSite intranetSite = new SPSite(siteUrl))
                                    {
                                        //writeLog(logfile, "Debug - 1");
                                        using (SPWeb intranetWeb = intranetSite.OpenWeb())
                                        {
                                            //writeLog(logfile, "Debug - 2");
                                            SPList ListSetting = intranetWeb.Lists[ListSettingArchiveDocument];
                                            //writeLog(logfile, "Debug - 2a"); 
                                            foreach (SPListItem setting in ListSetting.Items)
                                            {
                                                try
                                                {
                                                    #region Get email notification information for each document library
                                                    string doclib = setting["Title"].ToString();
                                                    string notifikasi = setting["Notifikasi Email"].ToString();
                                                    //writeLog(logfile, "Debug - 2b"); 
                                                    string mailto = "";
                                                    var penerimaemail = (SPFieldUserValueCollection)setting["Penerima Email"];
                                                    foreach (SPFieldUserValue penerima in penerimaemail)
                                                    {
                                                        if (String.IsNullOrEmpty(mailto))
                                                            mailto = penerima.User.Email;
                                                        else
                                                            mailto += ";" + penerima.User.Email;
                                                    }
                                                    //writeLog(logfile, "Debug - 2c"); 
                                                    if (notifikasi.Equals("True"))
                                                    {
                                                        SPList CobjLists = intranetWeb.Lists[doclib.Replace("%20", " ")];
                                                        //writeLog(logfile, "Debug - 2d"); 
                                                        foreach (SPContentType ContentType in CobjLists.ContentTypes)
                                                        {
                                                            try
                                                            {
                                                                #region Get All Documents which will be expired in xx days
                                                                contenttype = ContentType.Name;
                                                                writeLog(logfile, "Start Sending Archived Date Notification for " + contenttype + " documents.");
                                                                if (/*!contenttype.Equals("Document") && */!contenttype.Equals("Folder"))
                                                                {
                                                                    //writeLog(logfile, "Debug - " + ContentType.Name);
                                                                    //writeLog(logfile, "Debug - 3");

                                                                    lblStatus.Text = "Start Send Notification Archived Date " + contenttype + " Document...";
                                                                    writeLog(logfile, "Start Send Notification Archived Date " + contenttype + " Document - " + todaydate.ToShortDateString());
                                                                    SPListItemCollection CobjItemcoll;
                                                                    SPQuery CobjQuery = new SPQuery();

                                                                    CobjQuery.ViewAttributes = "Scope='Recursive'";
                                                                    String Cstrquerry = "";
                                                                    Cstrquerry = @"  <Where>
                                                                                        <And>
                                                                                            <Eq>
                                                                                                <FieldRef Name='ContentType' />
                                                                                                <Value Type='Computed'>" + contenttype + @"</Value>
                                                                                            </Eq>   
                                                                                            <Eq>
                                                                                                <FieldRef Name='columnname' />
                                                                                                <Value Type='DateTime' IncludeTimeValue='False'>keyword</Value>
                                                                                            </Eq> 
                                                                                        </And>                                     
                                                                                    </Where>";
                                                                    string columnname = "Archived Date";
                                                                    //writeLog(logfile, "Debug - " + columnname);
                                                                    Cstrquerry = Cstrquerry.Replace("columnname", columnname.Replace(" ", "_x0020_"));
                                                                    Cstrquerry = Cstrquerry.Replace("keyword", SPUtility.CreateISO8601DateTimeFromSystemDateTime(notifydate));
                                                                    CobjQuery.Query = Cstrquerry;
                                                                    //writeLog(logfile, "Debug - 4");

                                                                    CobjItemcoll = CobjLists.GetItems(CobjQuery);
                                                                    //writeLog(logfile, "Debug - 5");

                                                                    foreach (SPListItem doc in CobjItemcoll)
                                                                    {
                                                                        try
                                                                        {
                                                                            string file = doc.File.Name;
                                                                            string folder = doc.File.Url.Substring(0, doc.File.Url.LastIndexOf('/'));
                                                                            string link = intranetWeb.Url + "/" + doc.File.Url;
                                                                            listdokumen.Rows.Add(file, folder, link, mailto);
                                                                            //writeLog(logfile, "Debug - " + file + folder + link);
                                                                            //writeLog(logfile, "Debug - 6");
                                                                            writeLog(logfile, doc.File.Name + " akan diarsip dalam " + day + " hari");
                                                                        }
                                                                        catch (Exception ex)
                                                                        {
                                                                            writeLog(logfile, doc.File.Name + ": " + ex.Message);
                                                                        }
                                                                    }
                                                                    //writeLog(logfile, "Debug - 7");
                                                                }
                                                                #endregion

                                                                #region Send Email
                                                                if (listdokumen.Rows.Count > 0)
                                                                {
                                                                    List<DataTable> listdokumenbypenerima = listdokumen.AsEnumerable()
                                                                                            .GroupBy(row => row.Field<string>("MailTo"))
                                                                                            .Select(g => g.CopyToDataTable())
                                                                                            .ToList();
                                                                    foreach (DataTable dokumenbypenerima in listdokumenbypenerima)
                                                                    {
                                                                        MailTo = dokumenbypenerima.Rows[0]["MailTo"].ToString().Split(';').ToList();
                                                                        string Subject = "Email Notifikasi Archived Date";
                                                                        string Body = "<b>Dear " + contenttype + " Team,</b>" +
                                                                                    "<br></br>" +
                                                                                    "Berikut list Dokumen yang akan di-Archived dalam waktu " + day + " hari kedepan :" +
                                                                                    "<br></br>";
                                                                        Body += "<table border=\"1\">" +
                                                                                "<tr>" +
                                                                                "<td>No.</td>" +
                                                                                "<td>File</td>" +
                                                                                "<td>Folder</td>" +
                                                                                "<td>Link</td>" +
                                                                                "</tr>";
                                                                        int count = 0;
                                                                        foreach (DataRow dokumen in dokumenbypenerima.Rows)
                                                                        {
                                                                            count++;
                                                                            Body += "<tr>";
                                                                            Body += "<td>" + count.ToString() + ". </td>";
                                                                            Body += "<td>" + dokumen["File"] + "</td>";
                                                                            Body += "<td>" + dokumen["Folder"] + "</td>";
                                                                            Body += "<td><a href=\"" + dokumen["Link"] + "\">" + dokumen["Link"] + "</a></td>";
                                                                            Body += "</tr>";
                                                                        }
                                                                        Body += "</table><br></br>";
                                                                        Body += "Klik link berikut untuk membuka " + contenttype + " Sub-site :" +
                                                                                    "<br></br>" +
                                                                                    "<a href='" + intranetWeb.Url + "'>" + intranetWeb.Title + "</a>" +
                                                                                    "<br></br>" +
                                                                                    "<i>GT Document Management System Sharepoint</i>";
                                                                        sendMail(Subject, Body, MailFrom, Password, MailTo.ToArray());
                                                                        //writeLog(logfile, Body);
                                                                    }
                                                                }
                                                                //writeLog(logfile, "Debug - 10");
                                                                writeLog(logfile, "Send Notification Archived Date " + contenttype + " Success");
                                                                lblStatus.Text = "Send Notification Archived Date " + contenttype + " Document Success";

                                                                listdokumen.Clear();
                                                                #endregion
                                                            }
                                                            catch (Exception ex)
                                                            {
                                                                writeLog(logfile, ContentType.Name + ": " + ex.Message);
                                                            }
                                                        }
                                                    }
                                                    #endregion
                                                }
                                                catch (Exception ex)
                                                {
                                                    writeLog(logfile, setting["Title"].ToString() + ": " + ex.Message);
                                                }
                                            }
                                            //writeLog(logfile, "Debug - 9");  
                                        }
                                    }
                                    #endregion
                                }
                            }
                            else
                            {
                                writeLog(logfile, "List " + listArchivingSitesName + " not found at " + SiteURL);
                            }
                            #endregion
                        }
                    }
                });
            }
            catch (Exception ex)
            {
                writeLog(logfile, ex.Message);
                lblStatus.Text = "Send Notification Archived Date " + contenttype + " Failed : " + ex.Message;
            }
            #endregion
        }

        private void RunExpiredNotification()
        {
            int firstExpiredNotificationDay = 30;
            int secondExpiredNotificationDay = 14;
            // Start sending notifications for documents which will be expired in x days
            string firstExpiredNotification = ConfigurationManager.AppSettings["FirstExpiredNotification"];
            string secondExpiredNotification = ConfigurationManager.AppSettings["SecondExpiredNotification"];

            try
            {
                firstExpiredNotificationDay = int.Parse(firstExpiredNotification);
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message + " " + ex.StackTrace;
            }

            try
            {
                secondExpiredNotificationDay = int.Parse(secondExpiredNotification);
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message + " " + ex.StackTrace;
            }

            NotifyExpiredDocument(firstExpiredNotificationDay);
            NotifyExpiredDocument(secondExpiredNotificationDay);
        }

        private void RunArchivedNotification()
        {
            int firstArchiveedNotificationDay = 30;
            int secondArchivedNotificationDay = 14;
            // Start sending notifications for documents which will be archived in x days
            string firstArchivedNotification = ConfigurationManager.AppSettings["FirstArchivedNotification"];
            string secondArchivedNotification = ConfigurationManager.AppSettings["SecondArchivedNotification"];

            try
            {
                firstArchiveedNotificationDay = int.Parse(firstArchivedNotification);
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message + " " + ex.StackTrace;
            }

            try
            {
                secondArchivedNotificationDay = int.Parse(secondArchivedNotification);
            }
            catch (Exception ex)
            {
                lblStatus.Text = ex.Message + " " + ex.StackTrace;
            }

            NotifyArchiveDocument(firstArchiveedNotificationDay);
            NotifyArchiveDocument(secondArchivedNotificationDay);
        }
        #endregion
        #region minimize

        private void FrmDocumentArchiving_Resize(object sender, EventArgs e)
        {
            if (FormWindowState.Minimized == this.WindowState)
            {
                notifyIcon1.Visible = true;
                this.Hide();
            }
            else if (FormWindowState.Normal == this.WindowState)
            {
                notifyIcon1.Visible = false;
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            notifyIcon1.Visible = false;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }

        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            notifyIcon1.Visible = false;
            this.Show();
            this.WindowState = FormWindowState.Normal;
        }
        #endregion
    }
}
