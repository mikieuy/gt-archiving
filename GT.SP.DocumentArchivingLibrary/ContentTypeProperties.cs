﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.SP.DocumentArchivingLibrary
{
    public class ContentTypeProperties
    {
        public string Description { get; set; }
        public string Group { get; set; }
        public bool? IsHidden { get; set; }
        public bool? IsReadOnly { get; set; }
        public string NewDocumentControl { get; set; }
        public bool? RequireClientRenderingOnNew { get; set; }
        public bool? IsSealed { get; set; }
    }
}
