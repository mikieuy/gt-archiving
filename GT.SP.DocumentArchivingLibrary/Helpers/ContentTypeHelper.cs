﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using System.Reflection;

namespace GT.SP.DocumentArchivingLibrary.Helpers
{
    public class ContentTypeHelper
    {
        public static void AddContentType(SPWeb spWeb, SPList spList, ContentType contentType)
        {
            // get appropriate parent content type
            SPContentType parentContentType = spWeb.AvailableContentTypes[contentType.ParentContentTypeName];
            if (parentContentType != null)
            {
                if (spList.ContentTypes[contentType.Name] == null)
                {
                    spList.ContentTypesEnabled = true;

                    // create new content type
                    SPContentType spContentType = new SPContentType(parentContentType, spList.ContentTypes, contentType.Name);
                    // set content type id
                    SetContentTypeId(spContentType, contentType.Id);
                    // set content type properties
                    SetContentTypeProperties(spContentType, contentType.Properties);

                    // add fields to conent type
                    foreach (FieldRef fieldRef in contentType.FieldRefs)
                    {
                        if (spList.Fields.ContainsField(fieldRef.Name))
                        {
                            SPField targetField = fieldRef.ID.HasValue ? spList.Fields[fieldRef.ID.Value] : spList.Fields.GetFieldByInternalName(fieldRef.Name);
                            if (targetField != null && !spContentType.Fields.ContainsField(fieldRef.Name))
                            {
                                SPFieldLink fLink = new SPFieldLink(targetField);
                                SetFieldRefProperties(fLink, fieldRef);
                                spContentType.FieldLinks.Add(fLink);
                            }
                        }
                        // Uncomment these lines if you want to test it on a console apps
                        //else
                        //    Console.Write(string.Format("Couldn't find field {0} in the list {1}", fieldRef.Name, spList.Title));
                    }

                    // set content type form urls
                    SetContentTypeFormUrls(spContentType, contentType.FormUrls);

                    // save changes
                    spList.ContentTypes.Add(spContentType);
                    spContentType.Update();
                    spList.Update();
                }
                //else
                //    Console.Write(string.Format("Content type {0} already exists in the list {1}", contentType.Name, spList.Title));
            }
            //else
            //    Console.Write(string.Format("Couldn't find the parent content type {0}", contentType.ParentContentTypeName));
        }

        public static void SetContentTypeFormUrls(SPContentType spContentType, ContentTypeFormUrls contentTypeFormUrls)
        {
            if (contentTypeFormUrls == null)
                return;

            if (contentTypeFormUrls.NewFormUrl != null)
                spContentType.NewFormUrl = contentTypeFormUrls.NewFormUrl;

            if (contentTypeFormUrls.DisplayFormUrl != null)
                spContentType.DisplayFormUrl = contentTypeFormUrls.DisplayFormUrl;

            if (contentTypeFormUrls.EditFormUrl != null)
                spContentType.EditFormUrl = contentTypeFormUrls.EditFormUrl;
        }

        public static void SetContentTypeProperties(SPContentType spContentType, ContentTypeProperties cntProps)
        {
            if (cntProps == null)
                return;

            if (cntProps.Description != null)
                spContentType.Description = cntProps.Description;
            if (cntProps.Group != null)
                spContentType.Group = cntProps.Group;
            if (cntProps.IsHidden != null)
                spContentType.Hidden = cntProps.IsHidden.Value;
            if (cntProps.IsReadOnly != null)
                spContentType.ReadOnly = cntProps.IsReadOnly.Value;
            if (cntProps.IsSealed != null)
                spContentType.Sealed = cntProps.IsSealed.Value;
            if (cntProps.NewDocumentControl != null)
                spContentType.NewDocumentControl = cntProps.NewDocumentControl;
            if (cntProps.RequireClientRenderingOnNew != null)
                spContentType.RequireClientRenderingOnNew = cntProps.RequireClientRenderingOnNew.Value;
        }

        public static void SetContentTypeId(SPContentType spContentType, SPContentTypeId contentTypeId)
        {
            try
            {
                FieldInfo fi = typeof(SPContentType).GetField("m_id", BindingFlags.NonPublic | BindingFlags.Instance);
                if (fi != null)
                    fi.SetValue(spContentType, contentTypeId);
                //else
                //    Console.Write("Couldn't set content type id!");
            }
            catch (Exception ex)
            {
                //Console.Write(string.Format("Couldn't set content type id! {0}", ex.Message));
            }
        }

        public static void SetFieldRefProperties(SPFieldLink fLink, FieldRef fieldRef)
        {
            if (fieldRef == null)
                return;

            if (fieldRef.DisplayName != null)
                fLink.DisplayName = fieldRef.DisplayName;
            if (fieldRef.IsHidden != null)
                fLink.Hidden = fieldRef.IsHidden.Value;
            if (fieldRef.IsRequired != null)
                fLink.Required = fieldRef.IsRequired.Value;
            if (fieldRef.IsReadOnly != null)
                fLink.ReadOnly = fieldRef.IsReadOnly.Value;

            if (fieldRef.Aggregation != null)
                fLink.AggregationFunction = fieldRef.Aggregation;
            if (fieldRef.Customization != null)
                fLink.Customization = fieldRef.Customization;
            if (fieldRef.PIAttribute != null)
                fLink.PIAttribute = fieldRef.PIAttribute;
            if (fieldRef.PITarget != null)
                fLink.PITarget = fieldRef.PITarget;
            if (fieldRef.PrimaryPIAttribute != null)
                fLink.PrimaryPIAttribute = fieldRef.PrimaryPIAttribute;
            if (fieldRef.PrimaryPITarget != null)
                fLink.PrimaryPITarget = fieldRef.PrimaryPITarget;
            if (fieldRef.Node != null)
                fLink.XPath = fieldRef.Node;
        }
    }
}
