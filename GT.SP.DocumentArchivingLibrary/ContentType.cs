﻿using Microsoft.SharePoint;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.SP.DocumentArchivingLibrary
{
    public class ContentType
    {
        public string ParentContentTypeName { get; set; }
        public string Name { get; set; }
        public List<FieldRef> FieldRefs { get; set; }

        public ContentTypeFormUrls FormUrls { get; set; }
        public ContentTypeProperties Properties { get; set; }
        public SPContentTypeId Id { get; set; }
    }
}
