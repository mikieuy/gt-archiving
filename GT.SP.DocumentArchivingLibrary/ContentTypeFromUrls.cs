﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GT.SP.DocumentArchivingLibrary
{
    public class ContentTypeFormUrls
    {
        public string DisplayFormUrl { get; set; }
        public string NewFormUrl { get; set; }
        public string EditFormUrl { get; set; }
    }
}
